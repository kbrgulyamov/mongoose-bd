const mongoose = require('mongoose')

const Todos = mongoose.Schema({
    // Связка
    authorId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Users",
        required: true
    },
    title: {
        type: String
    },
    description: {
        type: String
    },
    isDone: {
        type: Boolean
    }
})

module.exports = mongoose.model('Todos', Todos)
