module.exports = {
    todos: require("./todos.js"),
    users: require("./users.js"),
    products: require("./products.js"),
    categories: require("./categories")
}