const mongoose = require('mongoose')

const Products = mongoose.Schema({
      // Связка
      UserId: {
            type: mongoose.Schema.Types.ObjectId,
            ref: "Categories",
            required: true
      },
      cotegoryId: {
            type: mongoose.Schema.Types.ObjectId,
            ref: "Categories"
      },
      title: {
            type: String
      },
      description: {
            type: String
      },
      price: {
            type: Number
      },
      sale: {
            type: false
      }
})

module.exports = mongoose.model('Products', Products)