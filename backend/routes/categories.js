const express = require("express")
const router = express.Router()
const Categories = require("../models/categories")

router.get("/", async (req, res) => {

      try {
            let body = await Categories.find()

            res.json({
                  ok: true,
                  message: 'get all categories',
                  body: body,

            })
      } catch (error) {
            console.log(error);

            res.json({
                  ok: false,
                  message: "some error",
                  error
            })
      }
})


// Создание одной записи
router.post("/", async (req, res) => {
      try {
            Categories.create(req.body, async (error, data) => {
                  if (error) {
                        console.log(error);

                        res.json({
                              ok: false,
                              message: "Categories fucked!",
                              error
                        })
                  } else {
                        res.json({
                              ok: true,
                              message: "Categories registered!",
                              element: data
                        })
                  }
            })
      } catch (error) {
            console.log(error);

            res.json({
                  ok: false,
                  message: "Some error",
                  error
            })
      }
})

module.exports = router
