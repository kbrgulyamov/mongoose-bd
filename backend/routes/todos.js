const express = require("express")
const router = express.Router()
// Подключаем модель к раутеру
const Todos = require("../models/todos")

// Создание одной записи
router.post("/", async (req, res) => {
    // Тут идет метод создания записи в БД
    try {
        // Callback пишется при создании
        // Request.body - это объект с фронта со всеми ключами
        Todos.create(req.body, async (error, data) => {
            if (error) {
                console.log(error);

                res.json({
                    ok: false,
                    message: "Error inside callback function WTF!",
                    error
                })
            } else {
                res.json({
                    ok: true,
                    message: "Element created!",
                    // Значение берем из аргумента функции
                    // Тут мы получаем только что созданный элемент
                    element: data
                })
            }
        })
    } catch (error) {
        console.log(error);

        res.json({
            ok: false,
            message: "Some error",
            error
        })
    }
})

router.get("/", async (req, res) => {
    try {
        // Связываем пользователей и задачи
        let body = await Todos.find().populate("authorId")

        res.json({
            ok: true,
            message: "Get all todos with users inside!",
            body
        })
    } catch (error) {
        console.log(error);

        res.json({
            ok: false,
            message: "Some error",
            error
        })
    }
})

// раут на изменение
router.patch("/", async (req, res) => {
})

// раут на удаление
router.delete("/:id", async (req, res) => {
    Todos.findByIdAndDelete(req.params.id, (error, data) => {
        if (error) {
            console.log(error);

            res.json({
                ok: false,
                message: "Deleting fucked!",
                error
            })
        } else {
            res.json({
                ok: true,
                message: "Deleted successfully!",
                data
            })
        }
    })
})

module.exports = router
