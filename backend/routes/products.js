const express = require('express')
const router = express.Router()
const Products = require("../models/products")


router.post("/", async (req, res) => {
      // Тут идет метод создания записи в БД
      try {
            // Callback пишется при создании
            // Request.body - это объект с фронта со всеми ключами
            Products.create(req.body, async (error, data) => {
                  if (error) {
                        console.log(error);

                        res.json({
                              ok: false,
                              message: "Error inside callback function WTF!",
                              error
                        })
                  } else {
                        res.json({
                              ok: true,
                              message: "Element created!",
                              // Значение берем из аргумента функции
                              // Тут мы получаем только что созданный элемент
                              element: data
                        })
                  }
            })
      } catch (error) {
            console.log(error);

            res.json({
                  ok: false,
                  message: "Some error",
                  error
            })
      }
})



router.get("/", async (req, res) => {
      try {
            // Связываем пользователей и задачи
            let body = await Products.find().populate("UserId").populate('cotegoryId')

            res.json({
                  ok: true,
                  message: "Get all products with categories inside!",
                  body
            })
      } catch (error) {
            console.log(error);

            res.json({
                  ok: false,
                  message: "Some error",
                  error
            })
      }
})

// раут на изменение
router.patch("/", async (req, res) => {
})


module.exports = router